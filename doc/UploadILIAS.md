# Hinweise zum Hochladen Ihres Protokolls auf ILIAS

Laden Sie alle Versionen Ihres Protokolls zu Dokumentationszwecken aufs [ILIAS](https://ilias.studium.kit.edu/login.php?client_id=produktiv&cmd=force_login&lang=en)-System hoch. Wir haben hierzu unter den Gruppenbezeichnung **Mo01**, **Mo02**, **…** Bereiche, für alle Montags- und Donnerstagsgruppen eingerichtet, in denen Sie entsprechende Rechte zum *upload* haben sollten. Hinterlegen Sie dort das *pdf*-Dokument des jeweiligen Versuchs, wie Sie es aus dem Export des Jupyter-Servers erhalten. Der Dateiname sollte aus dem Versuchsnamen und der Endung *.pdf* bestehen (z.B. ```Vorversuch.pdf```). 

Die erste Version ($\mathrm{v0}$) Ihres Protokolls sollten Sie, als *initiale* Version, **als letzten Handgriff direkt nach Durchführung des Versuchs**, am Ende des Praktikumstages hochladen. Für alle weiteren Versionen gehen Sie wie folgt vor: 

- Klicken in dem Ihrer Gruppe zugeordneten ILIAS-Bereich aus dem Menü, rechts in der Zeile der hochgeladenen *initialen* Version Ihres Protokolls, den obersten Menüpunkt "Versionen", wie in **Bild 1** zu sehen, an: 

   <img src="../figures/ILIAS-version-0.png" alt="figures" style="zoom:100%;" />

  **Bild 1** (Menüpunkt "Versionen" auf dem ILIAS-System)

  ---

  Dadurch sollten Sie auf ein neues Fenster geführt werden, in dem Ihnen alle von dieser Datei angelegten Versionen angezeigt werden.  

- Klicken Sie die Fläche "Neue Version anlegen", wie in **Bild 2** zu sehen, an: 

   <img src="../figures/ILIAS-version-1.png" alt="figures" style="zoom:100%;" />

  **Bild 2** (Anlegen einer neuen Version des Protokolls auf dem ILIAS-System)

  ---

  Dadurch sollten Sie auf ein weiteres Fenster, wie in **Bild 3** gezeigt, geführt werden:

   <img src="../figures/ILIAS-version-2.png" alt="figures" style="zoom:100%;" />

  **Bild 3** (Abschluss des Vorgangs zum Hochladen einer neuen Version des Protokolls auf dem ILIAS-System)

  ---

  

- Das Feld "Titel" können Sie freilassen oder dort den Namen des Versuchs angeben. Sie können in einer kurzen "Zusammenfassung" Sinn und Zweck der hochzuladenden Datei dokumentieren. Wählen Sie die Datei aus, die Sie hochladen möchten und klicken Sie auf das Feld "Neue Version anlegen", um den Vorgang abzuschließen.

Die physische Versionierung der Protokolle sollte ausschließlich durch den *upload* als jeweils neue Version aufs ILIAS-System erfolgen.  

# Navigation

[Zurück zu P1-Praktikum](https://gitlab.kit.edu/kit/etp-lehre/p1-praktikum/students)
